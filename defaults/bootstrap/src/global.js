// Styles.
import "./scss/global.scss";

// Scripts.
import "./js/bootstrap";
import "./js/global";

// Images/Icons.
importAll(require.context("./images"));
importAll(require.context("./icons"));
