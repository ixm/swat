files_dir = "#{release_path}/docroot/sites/default/files"
shared_dir = "/mnt/shared/#{new_resource.name}/files"

directory "/mnt/shared/#{new_resource.name}"

bash "move files directory" do
    code <<-EOH
    mv #{files_dir} #{shared_dir}
EOH
    only_if { ::File.directory?(files_dir) }
    not_if { ::File.directory?(shared_dir) }
end

bash "rm sites directory" do
    code <<-EOH
    rm -rf #{files_dir}
EOH
    not_if { ::File.symlink?(files_dir) }
end

directory shared_dir

link files_dir do
    to shared_dir
end

bash "run composer install" do
    cwd release_path
    code <<-EOH
    composer install --no-dev --no-ansi --no-interaction
    EOH
end

bash "run frontend tasks" do
    user "imagex"
    group "imagex"
    cwd release_path
    environment({
        "HOME" => "/home/imagex"
    })
    code <<-EOH
    source /home/imagex/.nvm/nvm.sh
    nvm use 10.15.0
    ./vendor/bin/swat frontend
    EOH
end

bash "run drush tasks" do
    cwd release_path
    code <<-EOH
    ./vendor/bin/drush cr
    ./vendor/bin/drush updb -y
    ./vendor/bin/drush cim -y
    EOH
end
