<?php

namespace Imagex\Swat\Annotation;

/**
 * Create an annotation for SWAT updates.
 *
 * @Annotation
 * @Target({"METHOD","PROPERTY"})
 */
class Update {

  /** @Required */
  public $version;

  /**
   * @var string
   * @Required
   */
  public $description;

  /**
   * @var bool
   */
  public $required = FALSE;

}
