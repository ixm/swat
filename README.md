![SWAT 5 Logo](logo.png)
### (SUPER WICKED AWESOME TOOL)
SWAT is meant to be a helper for projects within ImageX. It provides a docksal setup, some templated Bitbucket pipelines and deployment helpers. And it's super wicked awesome.

## Requirements

SWAT requires the following to run:

1. Docksal - <https://docksal.io/>
2. Composer - <https://getcomposer.org/>
3. PHP >=8.1

##### Available Commands
Run `fin swat list` to get a listing of available commands.
## GETTING STARTED

Note: it's a good idea to run `fin upgrade` before starting to be sure you have the latest stacks.

New projects must use Drupal 10.

1. `fin project create --repo=git@bitbucket.org:ixm/swat-project-template.git --name=<project_name>`
2. Follow the prompts
3. `cd <project-name>`
4. Modify `docksal.env` for any stack changes
    1. Hardcode your PHP, DB, etc images to match your remote host
    2. If you've made changes, make sure to reset your stack!
5. Modify `swat/swat.yml` specific to project - [see more details here](https://bitbucket.org/ixm/swat/src/6.1.x/config/swat.yml)
    2. Add Remote Git repo (Acquia, pantheon, etc)
    3. Add Remote Git user/email ([see example below](#markdown-header-git))
6. Download your remote drush aliases: [Project Setup - Drush](https://imagex.atlassian.net/wiki/spaces/DEV/pages/2163114754/Project+Setup#%F0%9F%A7%99%E2%80%8D%E2%99%82%EF%B8%8F-Drush)
7. (If docroot changed) Update your location in:
    1. `phpcs.xml`
    2. `.tugboat/config.yml`
8. `fin swat install`
9. Export your config.
10. Setup your git remote
     1. `git remote add origin git@bitbucket.org:example/example.git`
     2. `git add . && git commit -m "PROJ-1: Initial Setup for <project>."`
     3. `git push -u origin master`

### Hosting Provider

See our internal documentation: [Configuring your hosting provider files](https://imagex.atlassian.net/wiki/spaces/DEV/pages/2163114754/Project+Setup#%F0%9F%92%BB-Configuring-your-hosting-provider-files)

#### Git:

Example of Git config in `swat.yml` for remote:

```
    git:
      default_branch: develop
      remotes:
        cloud: 'ixmcomponents@svn-4707.devcloud.hosting.acquia.com:ixmcomponents.git'
      user:
        name: 'ImageX Media'
        email: no-reply@imagexmedia.com
```

---

## Existing Projects

Existing projects generally should be manually upgraded; however, there are cases in which you might benefit from an automatic upgrade. See the [Upgrade Docs](UPGRADE.MD)

---

## Troubleshoot

1. `git@bitbucket.org: Permission denied (publickey).` on `fin project create`:
    1. Check if you have access on https://bitbucket.org/ixm/swat-project-template
    2. Run `fin system start` and try again
2. `Project healthcheck has timed out` on `fin project create`:
    1. Run the following commands:
       ```
       fin start
       fin init
        ```
