// Disable unused Bootstrap components using line comments.
window.Alert = require("bootstrap/js/dist/alert");
window.Button = require("bootstrap/js/dist/button");
window.Carousel = require("bootstrap/js/dist/carousel");
window.Collapse = require("bootstrap/js/dist/collapse");
window.Dropdown = require("bootstrap/js/dist/dropdown");
window.Modal = require("bootstrap/js/dist/modal");
window.Offcanvas = require("bootstrap/js/dist/offcanvas");
window.Popover = require("bootstrap/js/dist/popover");
window.Scrollspy = require("bootstrap/js/dist/scrollspy");
window.Tab = require("bootstrap/js/dist/tab");
window.Toast = require("bootstrap/js/dist/toast");
// See component.tooltip.
// window.Tooltip = require("bootstrap/js/dist/tooltip");
