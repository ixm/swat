<?php

/**
 * @file
 * Preprocess hook implementations.
 */

/**
 * Implements hook_preprocess_HOOK().
 */
function ${name}_preprocess_html(array &$variables): void {
  /** @var \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList */
  $themeExtensionList = \Drupal::service('extension.list.theme');
  $icons = file_get_contents($themeExtensionList->getPath('${name}') . '/public/assets/icons.svg');
  $variables['page_bottom']['icons'] = [
    '#type' => 'inline_template',
    '#template' => '<span class="d-none icons-sprite">' . $icons . '</span>',
  ];
}
