<?php

namespace Imagex\Swat\Commands;

use Imagex\Swat\SwatTasks;
use Robo\Contract\VerbosityThresholdInterface;
use Robo\Exception\TaskException;
use Symfony\Component\Yaml\Yaml;

/**
 * Command references for managing packages.
 *
 * @package Imagex\Swat\Commands
 */
class PackagesCommand extends SwatTasks {

  /**
   * Installs Composer dependencies.
   *
   * @command source:build:composer
   * @aliases sbc setup:composer:install
   */
  public function composerInstall() {
    $result = $this->taskExec(
      (DIRECTORY_SEPARATOR == "\\") ? 'set' : 'export' .
        " COMPOSER_EXIT_ON_PATCH_FAILURE=1 && composer install --ansi --no-interaction"
    )
      ->dir($this->getConfigValue('repo.root'))
      ->interactive($this->input()->isInteractive())
      ->setVerbosityThreshold(VerbosityThresholdInterface::VERBOSITY_VERBOSE)
      ->run();

    return $result;
  }

  /**
   * Sets modules to include/install.
   *
   * @command packages:init
   * @description Install composer dependencies for packages.
   */
  public function setupPackages() {
    $repo_root = $this->getConfigValue('repo.root');
    $default_packages = Yaml::parse(file_get_contents($repo_root . '/vendor/imagex/swat/config/modules.yml'));

    $choices = [];
    $developer = [];
    foreach ($default_packages['packages'] as $package => $modules) {
      if ($package === 'developer') {
        $choices[$package] = ucfirst($package) . " (Select additional modules)";
        $developer = array_combine($modules, $modules);
      }
      else {
        $choices[$package] = ucfirst($package) . " (" . implode(', ', $modules) . ")\n";
      }
    }

    $config['swat']['packages'] = $this->askChoice('Choose packages to install (i.e. common,search):', $choices, NULL, TRUE);

    // Developer choices.
    if (in_array('developer', $config['swat']['packages'])) {
      $config['swat']['developer']['modules'] = $this->askChoice('Choose additional modules to install (i.e. paragraphs,webform)', $developer, NULL, TRUE);
    }

    // Require the packages.
    $composer = $this->taskComposerRequire()->dir($repo_root);
    foreach ($config['swat']['packages'] as $package) {
      if (isset($default_packages['packages'][$package])) {
        $modules = $package === 'developer' ? $config['swat']['developer']['modules'] : $default_packages['packages'][$package];
        foreach ($modules as $module) {
          // Automatically add drupal prefix for drupal modules (without drupal/)
          if (!str_contains($module, '/')) {
            $module = 'drupal/' . $module;
          }
          $composer->dependency($module);
        }
      }
    }
    $composer->option('--no-update');
    $composer->run();
    $this->runComposerUpdate();

    // Write choices.
    $this->saveRepoConfig($config);
  }

  /**
   * Installs modules set in config.
   *
   * @command packages:install
   * @description Installs modules set in config.
   * @throws TaskException
   */
  public function installPackages() {
    $repo_root = $this->getConfigValue('repo.root');
    $default_packages = Yaml::parse(file_get_contents($repo_root . '/vendor/imagex/swat/config/modules.yml'));
    $loaded = $this->getConfigValue('swat.packages');

    if ($loaded) {
      $module_list = '';
      foreach ($loaded as $package) {
        if (isset($default_packages['packages'][$package])) {
          $modules = $package === 'developer' ? $this->getConfigValue('swat.developer.modules') : $default_packages['packages'][$package];
          if ($modules) {
            foreach ($modules as $module) {
              $module_list .= $module . ' ';
              // Add admin toolbar tools.
              if ($module == 'admin_toolbar') {
                $module_list .= 'admin_toolbar_tools ';
              }
            }
          }
        }
      }

      return $this->taskExecStack()
        ->stopOnFail()
        ->dir($repo_root)
        ->exec('drush pm-enable ' . $module_list . ' --no-interaction')
        ->run();
    }
  }

}
