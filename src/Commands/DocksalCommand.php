<?php

namespace Imagex\Swat\Commands;

use Imagex\Swat\SwatTasks;

/**
 * Defines commands in the "custom" namespace.
 */
class DocksalCommand extends SwatTasks {

  /**
   * Setup docksal extras.
   *
   * @command docksal:setup
   * @description Setup docksal extras.
   * @throws \Exception
   */
  public function setup() {
    $repo_root = $this->getConfigValue('repo.root');
    $docksal_dir = $repo_root . '/.docksal';

    // Create .docksal dir with defaults.
    $this->say("<info>Setting up docksal defaults...</info>");
    $docksal_defaults = $this->getConfigValue('swat.root') . '/defaults/docksal';
    $copy = $this->taskCopyDir([$docksal_defaults => $docksal_dir])
      ->run();
    if (!$copy->wasSuccessful()) {
      throw new \Exception("Unable to create .docksal directory.");
    }

    // Get docksal stack for the selected platform.
    $provider = $this->getConfigValue('swat.host');
    $docksal_stacks = [
      'acquia',
      'pantheon',
      'platformsh',
    ];
    $provider = (in_array($provider, $docksal_stacks) ? $provider : 'default');

    // Write simple config files.
    $docksal_env = $this->taskWriteToFile($docksal_dir . '/docksal.env')
      ->append()
      ->replace("DOCKSAL_STACK=default", "DOCKSAL_STACK=$provider")
      ->replace("DOCROOT=docroot", "DOCROOT={$this->getConfigValue('docroot_name')}");
    $docksal_env->run();
  }

  /**
   * Updates or removes the docksal theme helpers.
   *
   * @command docksal:theme
   * @description Updates or removes the docksal theme helpers.
   */
  public function themeAddition() {
    $theme_config = $this->getConfigValue('project.theme');
    $theme_command = $this->getConfigValue('repo.root') . '/.docksal/commands/frontend';

    // Reset.
    if (is_dir($theme_command)) {
      $docksal_defaults = $this->getConfigValue('swat.root') . '/defaults/docksal/commands/frontend';
      $this->taskCopyDir([$docksal_defaults => $theme_command])
        ->run();
    }

    if (!$theme_config['status']) {
      $this->taskDeleteDir($theme_command)->run();
    }
  }

}
