#!/bin/bash -ex
echo "Installing awscli"
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python get-pip.py
#apt-get install python-pip -y
pip install awscli
export AWS_ACCESS_KEY_ID=${ACCESS_KEY_ID}
export AWS_SECRET_ACCESS_KEY=${SECRET_KEY_ID}
export AWS_DEFAULT_REGION=${DEFAULT_REGION}
if [[ ${BITBUCKET_BRANCH} == 'production' ]]; then
    APP_ID=${PROD_APP_ID}
    LAYER_ID=${PROD_LAYER_ID}
elif [[ ${BITBUCKET_BRANCH} == 'staging' ]]; then
    APP_ID=${STAGING_APP_ID}
    LAYER_ID=${DEV_LAYER_ID}
elif [[ ${BITBUCKET_BRANCH} == 'develop' ]]; then
    APP_ID=${DEV_APP_ID}
    LAYER_ID=${DEV_LAYER_ID}
fi
echo "Deploying the application"
aws opsworks create-deployment \
  --stack-id ${STACK_ID} \
  --layer-id ${LAYER_ID} \
  --app-id ${APP_ID} \
  --command '{"Name":"deploy"}' \
  --comment "Bitbucket build - $BITBUCKET_BUILD_NUMBER, Commit $BITBUCKET_COMMIT"
