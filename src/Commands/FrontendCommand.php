<?php

namespace Imagex\Swat\Commands;

use Consolidation\AnnotatedCommand\CommandData;
use Consolidation\AnnotatedCommand\CommandResult;
use Imagex\Swat\SwatTasks;

/**
 * Commands for setting up frontend build tools.
 *
 * @package Swat
 */
class FrontendCommand extends SwatTasks {

  /**
   * Prevent frontend commands without webpack/yarn.
   *
   * @hook pre-command
   */
  public function initialize(CommandData $commandData): ?CommandResult {
    // Allow setup.
    if ($commandData->annotationData()->get('command') === 'frontend:setup') {
      return NULL;
    }

    // Allowed if it has webpack setup.
    return $this->noWebpackSetup();
  }

  /**
   * Sets up the frontend build tools.
   *
   * @command frontend:setup
   */
  public function frontendSetup(): void {
    // Remove possibly old existing frontend files.
    $oldFiles = [
      'package.json',
      'webpack.config.js',
      '.stylelintrc.json',
      '.eslintrc.json',
      'lerna.json',
      'nx.json',
    ];
    foreach ($oldFiles as $oldFile) {
      $this->taskFilesystemStack()->remove($this->getConfigValue('repo.root') . '/' . $oldFile)->run();
    }

    $this->taskFilesystemStack()->mirror(
      $this->getConfigValue('swat.root') . "/defaults/frontend",
      $this->getConfigValue('repo.root')
    )->run();

    // Replace docroot here for the case of pantheon/platform/etc.
    $files = [
      '.eslintrc',
      '.stylelintrc',
      '.yarnrc.yml',
      'FRONTEND.md',
      'package.json',
      'svgo.config.js',
      'turbo.json',
      'webpack.config.js',
    ];

    foreach ($files as $file) {
      $this->taskReplaceInFile($file)
        ->from(['${docroot_name}'])
        ->to([$this->getConfigValue('docroot_name')])
        ->run();
    }

    $this->frontendInstall();
  }

  /**
   * Install frontend requirements.
   *
   * @command frontend:install
   */
  public function frontendInstall(array $options = ['dev' => FALSE]): void {
    $repo_root = $this->getConfigValue('repo.root');
    $task = $this->taskExecStack()->stopOnFail()->dir($repo_root);

    if ($this->getYarnVersion() === 4) {
      $task
        ->exec("yarn install --check-resolutions")
        ->exec("yarn npm audit --all");
    }
    else {
      $task->exec($this->taskExec("yarn install --audit")
        ->rawArg($options['dev'] ? '' : '--production --frozen-lockfile')
        ->getCommand());
    }

    $task->run();
  }

  /**
   * Build frontend in production mode.
   *
   * @command frontend:build
   * @aliases frontend
   */
  public function frontendBuild(array $options = ['dev' => FALSE]): void {
    $this->frontendInstall($options);
    $repo_root = $this->getConfigValue('repo.root');
    $task = $this->taskExecStack()->stopOnFail()->dir($repo_root);

    if ($options['dev']) {
      $task->exec($this->taskExec("yarn build")
        ->rawArg($this->getYarnVersion() === 4 ? '--ui=stream' : '')
        ->getCommand());
    }
    else {
      $task->exec($this->taskExec("yarn build:production")
        ->rawArg($this->getYarnVersion() === 4 ? '--ui=stream' : '')
        ->getCommand());
    }
    $result = $task->run();
    if (!$result->wasSuccessful()) {
      throw new \Exception('Failed to build the frontend.');
    }
  }

  /**
   * Get the Yarn package manager version.
   */
  protected function getYarnVersion(): int {
    $yarn_version = $this
      ->taskExec("yarn --version")
      ->silent(TRUE)
      ->run()
      ->getOutputData();

    if (preg_match('/^4\./', $yarn_version)) {
      return 4;
    }
    return 1;
  }

}
