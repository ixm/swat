<?php

namespace Imagex\Swat\Commands;

use Acquia\Drupal\RecommendedSettings\Exceptions\SettingsException;
use Acquia\Drupal\RecommendedSettings\Settings;
use Imagex\Swat\Swat;
use Imagex\Swat\SwatTasks;
use Robo\Exception\TaskException;

/**
 * Main Setup commands.
 *
 * @package Swat
 */
class SetupCommand extends SwatTasks {

  /**
   * Main setup command for swat.
   *
   * @command setup:init
   * @description Setup Hosting, Docksal, Bitbucket, Theme, and Settings.
   * @aliases setup
   *
   * @option no-theme Choose to skip the theme options.
   */
  public function setupInit(array $options = ['no-theme' => FALSE]) {
    $repoRoot = $this->getConfigValue('repo.root');
    // Ensure swat dir exists.
    $this->taskFilesystemStack()
      ->mkdir($repoRoot . '/swat')
      ->run();

    // Update project prefix and grumphp.yml.
    if (!$prefix = $this->ask("Enter your project JIRA project prefix (e.g. 'SWAT123'):")) {
      return;
    }
    $this->saveRepoConfig(['project' => ['prefix' => $prefix]]);
    $this->saveRepoConfig(['swat' => ['version' => Swat::VERSION]]);

    // @todo this could be used elswhere.
    $this->taskFilesystemStack()
      ->copy(
        $this->getConfigValue('swat.root') . '/defaults/deploy/deploy-exclude-additions.txt',
        $this->getConfigValue('repo.root') . '/swat/deploy-exclude-additions.txt'
      )
      ->run();

    $commands = [
      'setup:settings' => [],
      'setup:validate' => ['--force' => TRUE],
      'hosting' => [],
      'bitbucket:pipelines' => [],
      'docksal:setup' => [],
      'theme:setup' => [],
      'packages:init' => [],
      'tugboat:setup' => [],
      'cypress:setup' => [],
    ];

    if ($options['no-theme']) {
      unset($commands['theme:setup']);
    }

    $this->invokeCommands($commands);

    // Replace grum prefix.
    $this->taskReplaceInFile($repoRoot . '/grumphp.yml')
      ->from('${prefix}')
      ->to($prefix)
      ->run();

    // All done.
    $this->say("Done.");
  }

  /**
   * Initialize Settings from acquia/drupal-recommended-settings.
   *
   * @command setup:settings
   * @description Sets up settings files.
   * @aliases settings
   */
  public function initSettings() {
    // Remove old file first.
    $this->taskFilesystemStack()
      ->remove($this->getConfigValue('docroot') . '/sites/default/settings.php')
      ->remove($this->getConfigValue('docroot') . '/sites/default/settings/local.settings.php')
      ->run();

    // @todo could support multisite.
    $settings = new Settings($this->getConfigValue('docroot'), 'default');

    // Database details.
    $dbSpec = [
      'drupal' => [
        'db' => [
          'database' => 'default',
          'username' => 'user',
          'password' => 'user',
          'host' => 'db',
          'port' => '3306',
        ],
      ],
    ];

    try {
      // Call generate method passing database details.
      $settings->generate($dbSpec);
    }
    catch (SettingsException $e) {
      echo $e->getMessage();
    }

    // Rename the includes.
    $this->taskFilesystemStack()
      ->copy(
        $this->getConfigValue('docroot') . '/sites/default/settings/default.includes.settings.php',
        $this->getConfigValue('docroot') . '/sites/default/settings/includes.settings.php',
        TRUE
      )
      ->run();

    $this->say("<info>Settings file updated!</info>");
  }

  /**
   * Initialize validation config/settings.
   *
   * @command setup:validate
   * @description Sets up validation files.
   *
   * @option force Choose to force install.
   */
  public function initValidate(array $options = ['force' => FALSE]) {
    $repoRoot = $this->getConfigValue('repo.root');
    // Iterate files in swat defaults validate and confirm before copy.
    $files = scandir($this->getConfigValue('swat.root') . '/defaults/validate');
    foreach ($files as $file) {
      if ($file == '.' || $file == '..') {
        continue;
      }
      if (!$options['force'] && file_exists("{$repoRoot}/{$file}") && !$this->confirm("$file exists, replace?")) {
        continue;
      }
      $this->taskFilesystemStack()
        ->copy(
          $this->getConfigValue('swat.root') . '/defaults/validate/' . $file,
          $repoRoot . '/' . $file,
          TRUE
        )
        ->run();
    }
  }

  /**
   * Installs a new site using SWAT.
   *
   * @command setup:install
   * @description Installs a new site using SWAT.
   * @aliases install
   * @throws TaskException
   */
  public function setupInstall() {
    $repo_root = $this->getConfigValue('repo.root');

    // Install. @todo this needs work.
    $this->taskExecStack()->stopOnFail()
      ->dir($repo_root)
      ->exec('drush si standard -y')
      ->run();

    // Install modules after.
    $this->invokeCommands(['packages:install']);

    // If we have a theme, install that too.
    $theme_config = $this->getConfigValue('project.theme');
    if ($theme_config['status']) {
      $this->taskExecStack()->stopOnFail()
        ->dir($repo_root)
        ->exec('drush theme-enable ' . $theme_config['name'])
        ->exec('drush config-set system.theme default ' . $theme_config['name'] . ' -y')
        ->run();
    }

    // Show message and ULI.
    $this->yell("Git in 'er ya crazy jamoke!");
    $this->taskExecStack()->stopOnFail()
      ->dir($repo_root)
      ->exec('./vendor/bin/drush uli')
      ->run();
  }

}
